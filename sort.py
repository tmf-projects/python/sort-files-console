import glob
import os
import shutil

main_folder = 'Sort'

files_dir_names = ['text', 'archives', 'executable',
                   'java', 'photo', 'torrent',
                   'music', 'video', 'special',
                   'browser_files', 'models']

files_extension = {
    files_dir_names[0]: [
        '.txt', '.py', '.doc',
        '.docx', '.pdf', '.xlsx',
        '.ppt', '.pptx', '.DOCX',
        '.rtf'
    ],
    files_dir_names[1]: [
        '.rar', '.zip', '.7z'
    ],
    files_dir_names[2]: [
        '.exe', '.msi'
    ],
    files_dir_names[3]: [
        '.jar'
    ],
    files_dir_names[4]: [
        '.jpg', '.png', '.gif'
    ],
    files_dir_names[5]: [
        '.torrent'
    ],
    files_dir_names[6]: [
        '.mp3', '.wav', '.ogg'
    ],
    files_dir_names[7]: [
        '.mp4'
    ],
    files_dir_names[8]: [
        '.pka', '.keystore', '.ini',
        '.s', '.dt', '.mwb', '.ttf'
    ],
    files_dir_names[9]: [
        '.js', '.html', '.json', '.css'
    ],
    files_dir_names[10]: [
        '.blend', '.blend1'
    ]
}

files_list = []


def scanning():
    os.chdir(os.getcwd())

    for extension in files_extension:
        for ext in files_extension.get(extension):
            for file in glob.glob("*"+ext):
                if os.path.abspath(file) == os.path.abspath(__file__):
                    continue
                files_list.append(os.path.abspath(file))
    sort()


def sort():
    if not os.path.isdir(main_folder):
        os.mkdir(main_folder)
        for directory in files_dir_names:
            if directory in os.listdir():
                shutil.move(directory, main_folder)
    os.chdir(main_folder)
    for directory in files_dir_names:
        if not os.path.isdir(directory):
            os.mkdir(directory)
    move()


def move():
    for directory in files_dir_names:
        for extension in files_extension:
            for ext in files_extension.get(extension):
                for file in files_list:
                    if os.path.splitext(os.path.basename(file))[1] == ext and ext in files_extension.get(directory):
                        shutil.move(file, directory)


if __name__ == '__main__':
    scanning()
